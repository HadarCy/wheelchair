#include <REG89C51.h>
//===========================================================
//program to conrol tow motors an across an H_bridge
void left ();
void right();
void leftRev ();
void rightRev();
void stop();
void forward();
void reverse();
void rightforward();
void leftforward();
void delay_ms(unsigned int x);
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void delay_ms(unsigned int x)    // delays x msec (at fosc=11.0592MHz)
{
    unsigned char j=0;
    while(x-- > 0)
    {
        for (j=0; j<125; j++) {;}
    }
}
//===================================================
void forward()
{

	P1_1=1;
	P1_2=1;
	CMOD=0x02;
	if (P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x33;			//20%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x33;
	CR=1;	
  P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;		

	}
	if(P1_5==0)
	{
		CMOD=0x02;	
	 CCAPM0=0x42;
	 CCAP0H=0x66;			//40%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x66;
  P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
	}
  if(P1_6==0)
	{
		CMOD=0x02;
	 CCAPM0=0x42;
   CCAP0H=0x80;			//50%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x80;
   P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
	}
	if(P1_7==0)
		{
			CMOD=0x02;	
	 CCAPM0=0x42;
   CCAP0H=0xCC;			//80%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xCC;
   P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
	CR=1;
}
}
//========================================================
void reverse()
{
	P1_1=0;
	P1_2=0;
	CMOD=0x02;
	if(P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x33;			//20%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x33;
	CR=1;
	 P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;	
}

	if(P1_5==0)
	{
		CMOD=0x02;	
	CCAPM0=0x42;
	CCAP0H=0x66;			//40%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x66;
	CR=1;
		 P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
		}

	if(P1_6==0)
{
	CCAPM0=0x42;
	CCAP0H=0x68;			//60%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x80;
	CR=1;
	 P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
}

	if(P1_7==0)
{
	CCAPM0=0x42;
	CCAP0H=0xcc;			//80%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xcc;    //80%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
		}
	}
//========================================================
void right()
{
	P1_1=1;
	P1_2=1;
	CMOD=0x02;
	if(P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xCC;			//20%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xe6;	//10%
	CR=1;
	P0_4=0; 
	P0_5=1;	
	P0_6=1;
	P0_7=1;		
	}
		if(P1_5==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x99;			//40%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xcc;	//20%
	CR=1;
	P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
	}
		if(P1_6==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x80;			//50%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xB3;	//30%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
	}

	if(P1_7==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x80;			//50%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xB3;	//30%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	

}
}
//========================================================
void left()
{
	P1_1=1;
	P1_2=1;
	CMOD=0x02;
	if(P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xe6;			//10%
	CR=1;
	CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xcc;	  //20%
	CR=1;
	P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;	;
    }
	if(P1_5==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xcc;			//20%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x99;	  //40%
	CR=1;
	P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
	}
	if(P1_6==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xB3;			//30%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x80;	  //50%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
	}
	if(P1_7==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xB3;			//30%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
    CCAP1H=0x80;	  //50%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
	}
}
//========================================================
void stop()
{
	P1_1=0;
	P1_2=0;
  P0_4=1;
	P0_5=1;
	P0_6=1;
	P0_7=1;
	CMOD=0x02;
	CCAPM0=0x42;
	CCAP0H=0xff;			//0%
	CR=1;

	CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xff;		//0%
	CR=1;
}
//========================================================
void rightRev()
{
	P1_1=1;
	P1_2=0;
	CMOD=0x02;
	if(P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x22;			//20%
	CR=1;

	CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xe6;	//10%
	CR=1;
	P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;	
}
	if(P1_5==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x99;			//40%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xcc;	//20%
	CR=1;
		P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
	}
		if(P1_6==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x80;			//50%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xB3;	//30%
	CR=1;
	P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
}
	if(P1_7==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x80;			//50%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xB3;	//30%
	CR=1;
		P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
}
}
//========================================================
void leftRev()
{
	P1_1=0;
	P1_2=1;
	CMOD=0x02;
	if(P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xe6; //10%
	CR=1;

	CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xB3;	//30%
	CR=1;
	P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;	
	}
		if(P1_5==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xCC; //20%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x99;	//40%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
	}
		if(P1_6==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xB3; //30%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x80;	//50%
	CR=1;
		P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
	}
		if(P1_7==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x99; //40%
	CR=1;

	 //CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x38;	//60%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
	}
}
//========================================================
void rightforward()
{
  P1_1=1;
	P1_2=1;
	CMOD=0x02;
	if(P1_0==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xe6; //10%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xB3;	//30%
	CR=1;
	P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;	
	}
		if(P1_5==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xCC; //20%
	CR=1;
	//CMOD=0x02;
	CCAPM1=0x42;
    CCAP1H=0x99;	//40%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	
	}
    if(P1_6==0)
	{
	CCAPM0=0x42;
	CCAP0H=0xB3; //30%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
    CCAP1H=0x80;	//50%
	CR=1;
	P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
	}
		if(P1_7==0)
	{
	CCAPM0=0x42;
	CCAP0H=0x99; //40%
	CR=1;

	 //CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0x68;	//60%
	CR=1;
  P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
	}
}
//========================================================
void leftforward()
{
  P1_1=1;
	P1_2=1;
	CMOD=0x02;
	if(P1_0==0)
    {
    CCAPM0=0x42;
    CCAP0H=0xB3; //30 %
   	CR=1;

	CMOD=0x02;
	CCAPM1=0x42;
  CCAP1H=0xe6; //10% 
	CR=1;
	P0_4=0;
	P0_5=1;	
	P0_6=1;
	P0_7=1;	
    }

	if(P1_5==0)
    {
    CCAPM0=0x42;
    CCAP0H=0x99; //40%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
    CCAP1H=0xcc; //20%
	CR=1;
   P0_4=1;
	P0_5=1;	
	P0_6=0;
	P0_7=1;	

    }
    if(P1_6==0)
    {
   CCAPM0=0x42;
   CCAP0H=0x80; //50%
	CR=1;

	//CMOD=0x02;
	CCAPM1=0x42;
   CCAP1H=0xCC; //30%
	CR=1;
  P0_4=1;
	P0_5=0;	
	P0_6=1;
	P0_7=1;	
    }
    if(P1_7==0)
    {
    CCAPM0=0x42;
    CCAP0H=0x68; //60%
	  CR=1;
	//CMOD=0x02;
	 CCAPM1=0x42;
    CCAP1H=0x40; //40%
	CR=1;
	P0_4=1;
	P0_5=1;	
	P0_6=1;
	P0_7=0;	
    }
}
//========================================================
void main()
{
	P1=0xff;
	P0=0xff;
	CR=0;
		stop();
			delay_ms(10);
	while(1)
	{
		while((P0_0==1)&(P0_1==1)&(P0_2==1)&(P0_3==1))
		{
		 stop();
		 delay_ms(10);
	  }
	
		while((P0_0==0)&(P0_1==1)&(P0_2==1)&(P0_3==1))
		{
			forward();
			delay_ms(10);
		}

    while((P0_0==1)&(P0_1==1)&(P0_2==1)&(P0_3==0))
		{
		 reverse();
		 delay_ms(10);
		}

		while ((P0_0==1)&(P0_1==0)&(P0_2==1)&(P0_3==1))
		{
		 left();
		 delay_ms(10);
		}

		while((P0_0==1)&(P0_1==1)&(P0_2==0)&(P0_3==1))
		{
		 right();
		 delay_ms(10);
		}

		while((P0_0==1)&(P0_1==0)&(P0_2==1)&(P0_3==0))
		{
		 leftRev();
		 delay_ms(10);
		}

	  while((P0_0==1)&(P0_1==1)&(P0_2==0)&(P0_3==0))
		{
		 rightRev();
		 delay_ms(10);
		}

    while((P0_0==1)&(P0_1==1)&(P0_2==1)&(P0_3==1))
		{
		 stop();
		 delay_ms(10);
	  }

		while((P0_0==0)&(P0_1==0)&(P0_2==1)&(P0_3==1))
		{
		  leftforward();
			delay_ms(10);
		}
    
		while((P0_0==0)&(P0_1==1)&(P0_2==0)&(P0_3==1))
		{
		 rightforward();
		 delay_ms(10);
		}
		
	}
}
